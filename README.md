# FlipClock.js

### Installation

FlipClock.js can be installed in the following ways:

#### Bower

	bower install FlipClock

#### Node (NPM)

	npm install flipclock

#### Download .zip

[Download .ZIP](https://github.com/objectivehtml/FlipClock/releases)

---

### Full Documentation

http://flipclockjs.com/

---

### New in v1.0

FlipClock originally was developed an example library for a computer science class that I taught. I never actually thought people would use it, let alone imagine how people would use it. It's been a long time coming, but FlipClock.js has been rewritten for a modern age with no dependencies.

- Rewritten ES6 Syntax
- No dependencies, pure vanilla JS
- Import with Webpack, Rollup, Browserify with the UMD build
- Mobile friendly with responsive CSS
- Supports negative values
- Supports alpha values
- All new CSS themes for different flip effects
- All new clock faces
- Extensible and customizable
- Unit testing with Jest

---

### Basic Usage

	import FlipClock from 'flipclock';

	const el = document.querySelector('.clock');

	const clock = new FlipClock(el, new Date, {
		face: 'HourCounter'
	});

### License

Copyright (c) 2018 Justin Kimbrell shared under MIT LICENSE
